package sample;

import io.opentracing.Tracer;

import com.uber.jaeger.Configuration;
import com.uber.jaeger.samplers.ConstSampler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@EnableScheduling
@SuppressWarnings("javadoc")
public class OpenTracingSpringBoot {

    @Bean
    public RestTemplate restTemplate(final RestTemplateBuilder restTemplateBuilder) {

        return restTemplateBuilder.build();
    }

    @Bean
    public Tracer jaegerTracer() {

        return new Configuration("spring-boot", new Configuration.SamplerConfiguration(ConstSampler.TYPE, 1),
                new Configuration.ReporterConfiguration()).getTracer();
    }

    public static void main(final String[] args) {

        SpringApplication.run(OpenTracingSpringBoot.class, args);
    }
}
