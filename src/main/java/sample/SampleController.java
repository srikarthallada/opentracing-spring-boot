package sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@SuppressWarnings("javadoc")
public class SampleController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/simplerequest")
    public String simpleRequest() {

        return "Answer to simple request.";
    }

    @RequestMapping("/nestedrequest")
    public String nestedRequest() {

        final ResponseEntity<String> response = this.restTemplate.getForEntity("http://localhost:8080/simplerequest",
                String.class);
        return "Answer to nested request: " + response.getBody();
    }
}
