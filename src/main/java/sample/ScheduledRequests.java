package sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@SuppressWarnings("javadoc")
public class ScheduledRequests {

    @Autowired
    private RestTemplate restTemplate;

    @Scheduled(fixedDelay = 5000)
    public void scheduledRequest() {

        final ResponseEntity<String> response =
                this.restTemplate.getForEntity("http://localhost:8080/nowhere", String.class);
    }

}
